<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ArquivosRequest;
use App\Http\Controllers\Controller;

use App\Models\Arquivos;

class ArquivosController extends Controller
{
    public function index()
    {
        $registro = Arquivos::first();

        return view('painel.arquivos.edit', compact('registro'));
    }

    public function update(ArquivosRequest $request, Arquivos $registro)
    {
        try {
            $input = $request->all();

            if ($request->hasFile('ficha_de_saude_e_autorizacao')) {
                $input['ficha_de_saude_e_autorizacao'] = Arquivos::uploadFile('ficha_de_saude_e_autorizacao');
            }

            $registro->update($input);

            return redirect()->route('painel.arquivos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
