<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SenhaDaGaleriaRequest;
use App\Http\Controllers\Controller;

use App\Models\SenhaDaGaleria;

class SenhaDaGaleriaController extends Controller
{
    public function index()
    {
        $registro = SenhaDaGaleria::first();

        return view('painel.senha-da-galeria.edit', compact('registro'));
    }

    public function update(SenhaDaGaleriaRequest $request, SenhaDaGaleria $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.senha-da-galeria.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
