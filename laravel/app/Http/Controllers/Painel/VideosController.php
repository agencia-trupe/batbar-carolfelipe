<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Helpers\Tools;

class VideosController extends Controller
{
    public function index()
    {
        $registros = Video::ordenados()->get();

        return view('painel.videos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.videos.create');
    }

    public function store(VideosRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = Tools::videoThumb($request->get('tipo'), $request->get('codigo'));

            Video::create($input);

            return redirect()->route('painel.videos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Video $registro)
    {
        return view('painel.videos.edit', compact('registro'));
    }

    public function update(VideosRequest $request, Video $registro)
    {
        try {

            $input = $request->all();

            if ($input['codigo'] !== $registro->codigo || $input['tipo'] !== $registro->tipo) {
                $input['capa'] = Tools::videoThumb($input['tipo'], $input['codigo']);
            }

            $registro->update($input);

            return redirect()->route('painel.videos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Video $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.videos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
