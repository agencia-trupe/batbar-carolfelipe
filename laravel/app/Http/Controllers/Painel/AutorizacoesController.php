<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Autorizacao;

class AutorizacoesController extends Controller
{
    public function index()
    {
        $autorizacoes = Autorizacao::orderBy('nome', 'ASC')->groupBy('nome')->get();

        return view('painel.autorizacoes.index', compact('autorizacoes'));
    }

    public function show(Autorizacao $autorizacao)
    {
        $autorizacoes = Autorizacao::whereNome($autorizacao->nome)->get();
        $embarques    = $autorizacoes->lists('embarque')->unique();

        return view('painel.autorizacoes.show', compact('autorizacoes', 'embarques'));
    }
}
