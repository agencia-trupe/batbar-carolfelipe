<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\AutorizacoesRequest;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

use App\Models\Autorizacao;
use App\Models\Arquivos;
use App\Models\Foto;
use App\Models\Video;
use App\Models\SenhaDaGaleria;

class HomeController extends Controller
{
    public function entrada()
    {
        return view('frontend.entrada');
    }

    public function index()
    {
        $hoje   = Carbon::now();
        $evento = Carbon::createFromDate(2017, 8, 11);

        $dias   = $evento->diffInDays($hoje);

        return view('frontend.home', compact('dias'));
    }

    public function ficha()
    {
        $arquivo = Arquivos::first()->ficha_de_saude_e_autorizacao;

        return view('frontend.ficha', compact('arquivo'));
    }

    public function fichaPost(AutorizacoesRequest $request)
    {
        try {

            $ficha = [];
            $rg    = [];

            foreach ($request->file('ficha') as $arquivo) {
                $ficha[] = Autorizacao::uploadFile($arquivo);
            }

            foreach ($request->file('rg') as $arquivo) {
                $rg[] = Autorizacao::uploadFile($arquivo);
            }

            $dados = [
                'nome'     => request('nome'),
                'ficha'    => json_encode($ficha),
                'rg'       => json_encode($rg),
                'embarque' => request('embarque')
            ];

            Autorizacao::create($dados);

            \Mail::send('emails.autorizacao', $dados, function($message) use ($request)
            {
                $message
                    ->to(env('MAIL_AUTORIZACAO'), config('site.name'))
                    ->from(env('MAIL_USERNAME'), config('site.name'))
                    ->subject('Autorização');
            });

            return view('frontend.ficha', ['enviado' => true]);

        } catch (\Exception $e) {
            return back()->with('erro_interno', true)->withInput();
        }
    }

    public function info()
    {
        return view('frontend.info');
    }

    public function contatos()
    {
        return view('frontend.contatos');
    }

    public function levar()
    {
        return view('frontend.levar');
    }

    public function galeria(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('frontend.galeria');
        }

        if (request('senha') == SenhaDaGaleria::first()->senha) {
            $fotos  = Foto::ordenados()->get();
            $videos = Video::ordenados()->get();
            return view('frontend.galeria', compact('fotos', 'videos'));
        }

        return view('frontend.galeria', [
            'senha_invalida' => true
        ]);
    }
}
