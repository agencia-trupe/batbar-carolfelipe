<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'tipo' => 'required',
            'codigo' => 'required',
            'capa' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
