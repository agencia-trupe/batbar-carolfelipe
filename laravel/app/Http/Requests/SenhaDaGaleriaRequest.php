<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SenhaDaGaleriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'senha' => 'required',
        ];
    }
}
