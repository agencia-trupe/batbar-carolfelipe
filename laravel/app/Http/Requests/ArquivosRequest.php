<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArquivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ficha_de_saude_e_autorizacao' => 'mimes:pdf',
        ];
    }
}
