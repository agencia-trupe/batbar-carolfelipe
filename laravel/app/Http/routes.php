<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@entrada')->name('entrada');
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('documentacao-obrigatoria', 'HomeController@ficha')->name('ficha-de-saude');
    Route::post('documentacao-obrigatoria', 'HomeController@fichaPost')->name('ficha-de-saude.post');
    Route::get('embarque-e-retorno', 'HomeController@info')->name('info');
    Route::get('o-que-levar', 'HomeController@levar')->name('o-que-levar');
    Route::get('contatos', 'HomeController@contatos')->name('contatos');
    Route::get('galeria', 'HomeController@galeria')->name('galeria');
    Route::post('galeria', 'HomeController@galeria')->name('galeria.post');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('arquivos', 'ArquivosController', ['only' => ['index', 'update']]);
		Route::resource('senha-da-galeria', 'SenhaDaGaleriaController', ['only' => ['index', 'update']]);
        Route::resource('autorizacoes', 'AutorizacoesController');
        Route::resource('fotos', 'FotosController');
        Route::resource('videos', 'VideosController');

        // Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        // Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
