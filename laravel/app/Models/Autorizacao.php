<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Autorizacao extends Model
{
    protected $table = 'autorizacoes';

    protected $guarded = ['id'];

    public static function uploadFile($file) {
        $path = 'arquivos/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
