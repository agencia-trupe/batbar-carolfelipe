<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Arquivos extends Model
{
    protected $table = 'arquivos';

    protected $guarded = ['id'];

    public static function uploadFile($file) {
        $file = request()->file($file);

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
