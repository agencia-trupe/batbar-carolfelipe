<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SenhaDaGaleria extends Model
{
    protected $table = 'senha_da_galeria';

    protected $guarded = ['id'];

}
