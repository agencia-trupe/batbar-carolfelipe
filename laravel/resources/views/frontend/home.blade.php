@extends('frontend.common.template')

@section('content')

    <div class="home">
        @if($dias >= 1)
        <div class="contador">
            FALTA{{ $dias > 1 ? 'M' : '' }}
            <span>{{ $dias }}</span>
            DIA{{ $dias > 1 ? 'S' : '' }}
        </div>
        @endif

        <h2>A festa do nosso Bat e Bar Mitzva está chegando!</h2>
        <p>Será um final de semana com muitas atividades, esportes, aventuras e comemorações no acampamento NR (www.nr.com.br). O NR tem toda a infraestrutura necessária para receber nosso grupo com segurança e conforto.</p>
        <p>Todas as informações sobre a viagem estão disponíveis neste site. Documentação obrigatória, locais e horários de embarque, retorno, o que levar na mala, telefones de contato, etc.</p>
        <p>É muito importante seguir direitinho os passos do item Documentação Obrigatória, ninguém poderá embarcar sem a Ficha Médica/Autorização de Viagem preenchida e a cópia do RG enviada.</p>
        <p>Para as mães acompanharem os acontecimentos, vamos carregar as fotos no decorrer do final de semana na Galeria de Fotos.</p>
        <p>Qualquer dúvida ligue diretamente para nossa mae, Maria Claudia (Pimpa), o telefone dela e do NR estão no item Contatos.</p>
        <p>Esperamos você!</p>
    </div>

@endsection
