@extends('frontend.common.template')

@section('content')

    <div class="info">
        <h3>Embarque escola Stance Dual</h3>
        <h4>
            Sexta feira, dia 11 de Agosto<br>
            Horário 13:30h
        </h4>
        <p>A escola já está ciente que as crianças sairão mais cedo. Não esqueçam de autorizar na agenda.</p>
        <p>Não precisa levar lanche.</p>

        <h3>Embarque Hebraica</h3>
        <h4>
            Sexta feira, dia 11 de Agosto<br>
            Horário 14:30h
        </h4>
        <p>Os ônibus estarão estacionados no clube A Hebraica.</p>
        <p>Não precisa levar lanche.</p>

        <h3>Retorno</h3>
        <h4>
            Domingo, dia 13 de Agosto<br>
            Horario 18:00h
        </h4>
        <p>Todos os ônibus retornarão para o clube A Hebraica.</p>
    </div>

@endsection
