@extends('frontend.common.template')

@section('content')

    @if(isset($enviado) && $enviado == true)
    <p class="enviado">Autorização enviada com sucesso!</p>
    @else
    <form action="{{ route('ficha-de-saude.post') }}" method="POST" class="ficha" accept-charset="UTF-8" enctype="multipart/form-data">
        @if(session('erro_interno'))
        <div class="erro">Ocorreu um erro. Tente novamente.</div>
        @endif
        @if($errors->any())
        <div class="erro">Preencha todos os campos corretamente.</div>
        @endif
        {!! csrf_field() !!}

        <p>Para poder embarcar no ônibus do NR é necessário:  imprimir a Ficha Médica/Autorização de Viagem,  preencher e assinar, digitalizar e apertar o botão de upload. Faça a mesma coisa com o RG (digitalize e aperte o botão de upload). Depois, é só escrever seu nome, escolher o local de embarque e enviar. A DATA LIMITE para enviar a documentação é dia 4 DE JUNHO.</p>

        <div class="passo">
            <div class="descricao">
                <span>Passo 1</span>
                FAÇA O DOWNLOAD E IMPRIMA A FICHA
            </div>
            <a href="{{ asset('assets/pdfs/'.$arquivo) }}" class="btn-ficha" target="_blank">
                <img src="{{ asset('assets/img/layout/ico-download.png') }}" alt="">
                <p>DOWNLOAD DA FICHA DE SAÚDE E AUTORIZAÇÃO DE VIAGEM</p>
            </a>
        </div>

        <div class="passo">
            <div class="descricao">
                <span>Passo 2</span>
                PREENCHA, ASSINE E FAÇA UPLOAD DA FICHA
                <em>* OBRIGATÓRIO</em>
            </div>
            <div class="btn-ficha">
                <img src="{{ asset('assets/img/layout/ico-upload.png') }}" alt="">
                <p>UPLOAD DA FICHA DE SAÚDE E AUTORIZAÇÃO DE VIAGEM PREENCHIDA E ASSINADA</p>
                <input type="file" name="ficha[]" id="ficha" multiple required>
            </div>
            <div class="ficha-lista"></div>
        </div>

        <div class="passo">
            <div class="descricao">
                <span>Passo 3</span>
                ENVIE A CÓPIA DO RG DO(A) CONVIDADO(A)
                <em>* OBRIGATÓRIO</em>
            </div>
            <div class="btn-ficha">
                <img src="{{ asset('assets/img/layout/ico-upload.png') }}" alt="">
                <p>UPLOAD DO RG DO(A) CONVIDADO(A)</p>
                <input type="file" name="rg[]" id="rg" multiple required>
            </div>
            <div class="rg-lista"></div>
        </div>

        <p class="finalize">FINALIZE O ENVIO:</p>

        <input type="text" name="nome" placeholder="NOME DO(A) CONVIDADO(A)" required value="{{ old('nome') }}">
        <div class="radios">
            <span>Embarcando a partir de:</span>
            <label>
                <input type="radio" name="embarque" value="STANCE DUAL" @if(old('embarque') == 'STANCE DUAL') checked @endif required>
                STANCE DUAL
            </label>
            <label>
                <input type="radio" name="embarque" value="HEBRAICA" @if(old('embarque') == 'HEBRAICA') checked @endif required>
                HEBRAICA
            </label>
        </div>
        <input type="submit" value="ENVIAR">
    </form>
    @endif

@endsection
