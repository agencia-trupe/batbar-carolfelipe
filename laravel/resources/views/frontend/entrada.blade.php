@extends('frontend.common.template')

@section('content')

    <div class="entrada">
        <a href="{{ route('home') }}" class="btn-entrada">
            <div class="convite">
                <div class="parte-1"></div>
                <div class="parte-2"></div>
            </div>
            <p>clique para entrar</p>
        </a>
    </div>

@endsection
