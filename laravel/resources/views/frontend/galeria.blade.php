@extends('frontend.common.template')

@section('content')

    <div class="galeria">
        @if(!isset($fotos) || !isset($videos))
        <form action="{{ route('galeria.post') }}" method="POST">
            {!! csrf_field() !!}
            <p>Utilize a senha para acessar fotos do evento:</p>
            @if(isset($senha_invalida))
            <span>senha inválida</span>
            @endif
            <input type="password" name="senha" placeholder="senha">
            <input type="submit" value="VER GALERIA">
        </form>
        @else
        <div class="fotos">
            @foreach($fotos as $foto)
            <a href="{{ asset('assets/img/fotos/'.$foto->imagem) }}" class="fancybox" rel="galeria">
                <img src="{{ asset('assets/img/fotos/thumbs/'.$foto->imagem) }}" alt="">
            </a>
            @endforeach
        </div>
        @if(count($videos))
        <div class="videos">
            @foreach($videos as $video)
            <a href="{{ $video->tipo === 'youtube' ? 'https://youtube.com/embed/'.$video->codigo : 'https://player.vimeo.com/video/'.$video->codigo }}" class="fancybox-video">
                <img src="{{ asset('assets/img/videos/'.$video->capa) }}" alt="">
            </a>
            @endforeach
        </div>
        @endif
        @endif
    </div>

@endsection
