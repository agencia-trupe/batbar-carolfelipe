<nav class="menu">
    <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>
        NOSSA FESTA
    </a>
    <a href="{{ route('ficha-de-saude') }}" @if(Tools::isActive('ficha-de-saude*')) class="active" @endif>
        DOCUMENTAÇÃO OBRIGATÓRIA
    </a>
    <a href="{{ route('info') }}" @if(Tools::isActive('info')) class="active" @endif>
        EMBARQUE E RETORNO
    </a>
    <a href="{{ route('o-que-levar') }}" @if(Tools::isActive('o-que-levar')) class="active" @endif>
        O QUE LEVAR?
    </a>
    <a href="{{ route('contatos') }}" @if(Tools::isActive('contatos')) class="active" @endif>
        CONTATOS
    </a>
    <a href="{{ route('galeria') }}" @if(Tools::isActive('galeria*')) class="active" @endif>
        GALERIA DE FOTOS E VÍDEOS
    </a>
</nav>
