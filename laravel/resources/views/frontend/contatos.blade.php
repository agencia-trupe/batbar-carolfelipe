@extends('frontend.common.template')

@section('content')

    <div class="contatos">
        <p>Contatos do evento:</p>
        <p>NR São Paulo - <span>11 5090 7419</span></p>
        <p>NR Sapucaí - <span>35 3655 1043</span></p>
        <p>Maria Claudia - <span>11 97300 1458</span></p>
        <p>Marcelo - <span>11 99997 3306</span></p>
        <p class="info">
            Para mais informações sobre o local, visite o site do NR:<br>
            <a href="http://www.nr.com.br" target="_blank">www.nr.com.br</a>
        </p>
    </div>

@endsection
