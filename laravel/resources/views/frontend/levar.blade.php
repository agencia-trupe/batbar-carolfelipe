@extends('frontend.common.template')

@section('content')

    <div class="levar">
        <div class="print">
            <h3>{{ config('site.name') }}</h3>
            <h1>O QUE LEVAR?</h1>
        </div>
        <div class="lista">
            <div class="col">
                <h2>Cama</h2>
                <ul>
                    <li>1 cobertor</li>
                    <li>1 fronha</li>
                    <li>1 jogo de lençol ou saco de dormir</li>
                    <li>1 travesseiro</li>
                </ul>

                <h2>Vestuário diário</h2>
                <ul>
                    <li>5 camisetas</li>
                    <li>1 camiseta branca (Zim color)</li>
                    <li>1 calça jeans</li>
                    <li>2 conjuntos de moleton (calça e blusa)</li>
                    <li>5 pares de meias</li>
                    <li>4 shorts ou bermudas</li>
                    <li>2 pares de tênis</li>
                    <li>5 roupas íntimas</li>
                    <li>1 casaco</li>
                    <li>2 pijamas (1 para o café da manha do pijama)</li>
                    <li>roupa para trilha seca ou na lama (camiseta, calça ou bermuda resistente, tênis, meia)</li>
                </ul>
            </div>

            <div class="col">
                <h2>Banho e piscina</h2>
                <ul>
                    <li>1 toalha de banho</li>
                    <li>1 toalha de rosto</li>
                    <li>1 toalha de piscina</li>
                    <li>1 sandália de borracha</li>
                    <li>2 maiôs ou biquínis</li>
                </ul>

                <h2>Material de uso pessoal</h2>
                <ul>
                    <li>1 lanterna com pilhas</li>
                    <li>1 saco para roupa suja</li>
                    <li>sabonete e saboneteira</li>
                    <li>pente ou escova de cabelos</li>
                    <li>creme dental e escova de dentes</li>
                    <li>shampoo e condicionador</li>
                    <li>desodorante</li>
                    <li>protetor solar</li>
                    <li>repelente</li>
                    <li>cantil/garrafa para água</li>
                </ul>

                <h2>FESTA DO SÁBADO</h2>
                <ul>
                    <li>roupa para uma baladinha</li>
                </ul>
            </div>
        </div>

        <div class="orientacoes">
            <h2>ORIENTAÇÕES GERAIS</h2>
            <p>Aconselhamos identificar todas as peças de roupa e materiais de uso pessoal.</p>
            <p>Desaconselhamos levar: telefone celular, jóias, relógios, ipod, vídeo games portáteis, cd player e similares. Caso ocorra perda dos itens o NR não se responsabiliza pelos mesmos.</p>
            <p>Telefones celulares não têm bom funcionamento por causa da localização geográfica do NR.</p>
            <p>O NR possui enfermaria com atendimento 24 horas, medicamentos de uso geral e materiais para pequenos curativos.</p>
            <p>Os remédios que a criança deverá tomar durante o período do acampamento deverão ser levados em quantidade suficiente, com as devidas prescrições e entregues ao responsável do grupo já no embarque.</p>
        </div>

        <a href="javascript:window.print();">IMPRIMIR INFORMAÇÕES</a>
    </div>

@endsection
