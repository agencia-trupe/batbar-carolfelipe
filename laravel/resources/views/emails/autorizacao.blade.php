<!DOCTYPE html>
<html>
<head>
    <title>[{{ config('site.name') }}] Autorização</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        <strong>Nome:</strong>
        {{ $nome }}
    </p>

    <p>
        <strong>Ficha de Saúde e Autorização:</strong><br>
        @foreach(json_decode($ficha) as $arquivo)
        <a href="{{ asset('arquivos/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
        <br>
        @endforeach
    </p>

    <p>
        <strong>RG:</strong><br>
        @foreach(json_decode($rg) as $arquivo)
        <a href="{{ asset('arquivos/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
        <br>
        @endforeach
    </p>

    <p>
        <strong>Embarque:</strong>
        {{ $embarque }}
    </p>
</body>
</html>
