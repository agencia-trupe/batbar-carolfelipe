<ul class="nav navbar-nav">
    <li @if(str_is('painel.autorizacoes.*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.autorizacoes.index') }}">Autorizações</a>
    </li>
    <li @if(str_is('painel.fotos.*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.fotos.index') }}">Fotos</a>
    </li>
    <li @if(str_is('painel.videos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.videos.index') }}">Vídeos</a>
    </li>
	<li @if(str_is('painel.arquivos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.arquivos.index') }}">Arquivos</a>
	</li>
	<li @if(str_is('painel.senha-da-galeria*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.senha-da-galeria.index') }}">Senha da Galeria</a>
	</li>
    {{--<li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>--}}
</ul>
