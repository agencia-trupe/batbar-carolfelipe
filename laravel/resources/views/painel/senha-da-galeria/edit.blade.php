@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Senha da Galeria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.senha-da-galeria.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.senha-da-galeria.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
