@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('senha', 'Senha') !!}
    {!! Form::text('senha', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
