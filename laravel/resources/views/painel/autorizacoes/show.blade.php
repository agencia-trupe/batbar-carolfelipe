@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Autorizações</h2>
    </legend>

    <div class="form-group">
        <label>Nome do(a) aluno(a):</label>
        <div class="well">{{ $autorizacoes->first()->nome }}</div>
    </div>
    <div class="form-group">
        <label>Embarque:</label>
        <div class="well">
            @if(count($embarques) > 1)
            <?php $i = 1; ?>
            @foreach($embarques as $embarque)
                Envio {{ $i }}: {{ $embarque }}<br>
                <?php $i++; ?>
            @endforeach
            @else
            {{ $embarques[0] }}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label>Ficha de Saúde e Autorização:</label>
        <div class="well">
            @foreach($autorizacoes as $autorizacao)
                @foreach(json_decode($autorizacao->ficha) as $arquivo)
                <a href="{{ asset('arquivos/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                <br>
                @endforeach
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <label>RG:</label>
        <div class="well">
            @foreach($autorizacoes as $autorizacao)
                @foreach(json_decode($autorizacao->rg) as $arquivo)
                <a href="{{ asset('arquivos/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                <br>
                @endforeach
            @endforeach
        </div>
    </div>

    <a href="{{ route('painel.autorizacoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@endsection
