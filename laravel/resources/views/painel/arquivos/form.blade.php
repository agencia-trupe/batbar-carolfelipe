@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('ficha_de_saude_e_autorizacao', 'Ficha de saúde e autorização') !!}
    @if($registro->ficha_de_saude_e_autorizacao)
    <p>
        <a href="{{ url('assets/pdfs/'.$registro->ficha_de_saude_e_autorizacao) }}" target="_blank" style="display:block;">{{ $registro->ficha_de_saude_e_autorizacao }}</a>
    </p>
    @endif
    {!! Form::file('ficha_de_saude_e_autorizacao', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
