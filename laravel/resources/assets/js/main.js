(function() {
    'use strict';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-entrada').click(function(event) {
        event.preventDefault();

        var homeUrl = $(this).attr('href');

        $('.parte-1').addClass('animated rotateOutDownRight');

        setTimeout(function() {
            location.href = homeUrl;
        }, 950);
    });

    $('.fancybox').fancybox({
        padding: 10
    });

    $('.fancybox-video').fancybox({
        padding: 0,
        type: 'iframe',
        width: 800,
        height: 450,
        aspectRatio: true
    });

    $('#ficha').change(function(event) {
        event.preventDefault();

        if(!event.target.files) return;

        var $lista = $('.ficha-lista');

        $lista.html('');

        var files = event.target.files;
        for (var i = 0, qtd = files.length; i < qtd; i++) {
            $lista.append('<p>' + files[i].name + '</p>');
        }
    });

    $('#rg').change(function(event) {
        event.preventDefault();

        if(!event.target.files) return;

        var $lista = $('.rg-lista');

        $lista.html('');

        var files = event.target.files;
        for (var i = 0, qtd = files.length; i < qtd; i++) {
            $lista.append('<p>' + files[i].name + '</p>');
        }
    });
}());
