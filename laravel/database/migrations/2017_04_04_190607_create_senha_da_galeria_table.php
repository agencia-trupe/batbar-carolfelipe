<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSenhaDaGaleriaTable extends Migration
{
    public function up()
    {
        Schema::create('senha_da_galeria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('senha');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('senha_da_galeria');
    }
}
